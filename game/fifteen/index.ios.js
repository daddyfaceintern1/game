var React = require('react');
import {
  AppRegistry,
  StyleSheet,
  Navigator,
  Text,
  View
} from 'react-native';
import Signup from './src/pages/Signup';
import Login from './src/pages/Login';
import * as firebase from 'firebase';

var Game = require('./src/Game');

const firebaseConfig = {
	apiKey: "AIzaSyD-RSRbf0Xjn8gVn0fHnnpNv9hTORjoW2c",
    authDomain: "game-fed9a.firebaseapp.com",
    databaseURL: "https://game-fed9a.firebaseio.com",
    storageBucket: "game-fed9a.appspot.com",
};

// Initialize the firebase app here and pass it to other components as needed. Only initialize on startup.
const firebaseApp = firebase.initializeApp(firebaseConfig);

var fifteen = React.createClass({
  render: function() {
    return (
    	<Navigator
        initialRoute={{component: Login}}
        configureScene={() => {
          return Navigator.SceneConfigs.FloatFromRight;
        }}
        renderScene={(route, navigator) => {
          if(route.component){
            // Pass the navigator the the component so it can navigate as well.
            // Pass firebaseApp so it can make calls to firebase.
            return React.createElement(route.component, { navigator, firebaseApp});
          }
      }} />
    );
  },
});

AppRegistry.registerComponent('fifteen', () => fifteen);
