
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,
} from 'react-native';
const ListItem = require('./ListItem');
const StatusBar = require('./StatusBar');
import styles from './styles/baseStyles.js';
let number=1;
export default class example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uid:'',
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      })
    };
  }
  listenForItems(itemsRef) {
    this.props.firebaseApp.database().ref().orderByChild('count').on('value', (snap) => {

      // get children as an array
      var items = [];
      snap.forEach((uid) => {
        items.push({
           count: uid.val().count,
           email: uid.val().email,
          _key: uid.key,
           index: number,
        });
        number++;
      });

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });

    });
  }

  componentDidMount() {
    this.listenForItems(this.props.firebaseApp.database().ref());
  }

  componentWillMount()  {
   this.setState({uid: this.props.firebaseApp.auth().currentUser.uid});
  }

  render() {
    return (
      <View style={styles.licontainer}>
      	 <StatusBar title="Ranking List" />
         <ListView
          dataSource={this.state.dataSource}
          renderRow={this._renderItem.bind(this)}
          enableEmptySections={true}
          style={styles.listview}
          />

          <View style={{width: 400, height: 100,flex:1, flexDirection: 'row',alignItems:'center',justifyContent:'center'}}>
        <TouchableHighlight style={styles.transparentButton}>
          <Text style={styles.transparentButtonText}>Order by count</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.transparentButton}>
          <Text style={styles.transparentButtonText}>Order by time</Text>
        </TouchableHighlight>
        
          </View>
      </View>

    );
  }
    _renderItem(item) {
    const onPress = () => {
      Alert.alert(
        'Complete',
        null,
        [
          {text: 'Complete', onPress: (text) => this.itemsRef.child(item._key).remove()},
          {text: 'Cancel', onPress: (text) => console.log('Cancelled')}
        ]
      );
    };

    return (
      <ListItem item={item} onPress={onPress} />
    );
  }
}
AppRegistry.registerComponent('example', () => example);
