import React, {Component} from 'react';
import ReactNative from 'react-native';
import styles from './styles/baseStyles.js';
const { View, TouchableHighlight, Text } = ReactNative;

class ListItem extends Component {
  render() {
    return (
        <View style={styles.li}>
          <Text> {this.props.item.index}. {this.props.item.email} : {this.props.item.count} </Text>
        </View>
    );
  }
}

module.exports = ListItem;